# Thư viện tích hợp Google Admob UMP


## UMP là gì? Tại sao phải tích hợp UMP?
- UMP giúp hỗ trợ việc xin quyền, giúp Admob pass được Policy về GDPR cho các vùng EU và UK.

- UMP chỉ để áp dụng cho Ads của Admob, tùy thuộc vào việc người chơi có Consent hay không mà Google sẽ phân phối hoặc không phân phối Ads của họ.

- Nên Init Mediation sau khi có kết quả UMP.

## User story
Nội dung tại mục này nhằm mục đích hướng dẫn người sử dụng chính sách về sự đồng ý của người dùng ở Liên minh châu Âu của Google. Một số điều cần chú ý khi tích hợp UMP:
1. Khi show UMP thì dừng game và khi tắt UMP thì game mới tiếp tục.
2. Được phép show UMP trong in-game.

Flowchart dưới đây mô tả cụ thể về flow trải nghiệm của user sau khi game đã tích hợp UMP.

![image](ump_user_flow.png)

Người dùng sẽ được phân thành 2 nhóm:
* Người dùng trong vùng EAA: khi người dùng mở app (show ATT đối với nền tảng iOS), app sẽ thực hiện load UMP:
  
  * Trường hợp load UMP thành công: app sẽ thực hiện show UMP và load Ads sau đó.
  * Trường hợp load UMP thất bại: app sẽ thực hiện load Ads và show như bình thường đến khi người dùng mở lại app thì sẽ check UMP lần nữa.

- Người dùng ngoài vùng EAA: khi người dùng mở app sẽ thực hiện load Ads luôn.

## Hướng dẫn sử dụng thư viện
Để thực hiện tích hợp UMP thông qua thư viện của Bravestars, thực hiện theo các bước chỉ dẫn dưới đây:
### Bước 1 - Import thư viện Bravestars
Trên Unity Editor, developer thực hiện import thư viện Bravestars tuần tự theo các bước sau:
* Trên thanh menu bar, click `Assets > Import Package > Custom Package ...`
* Import package `AdmobUMP_v9.2.1` 
* Package gồm 4 folder, nếu chưa thêm Admod 9.2.1 thì thực hiện import cả 4 folder. Nếu đã có Admob rồi thì chỉ cần import folder UMP.
* Nếu đã có External Dependency Manager rồi thì nên xóa các phiên bản cũ đi và chỉ dùng phiên bản mới nhất.
* LƯU Ý: phiên bản admob 9.2.1 cần tải Android SDK platform 31 and Android SDK Build Tools 30.0.3.

  ![image](resources/images/1_import_packages.png)

### Bước 2 - Config key cho Admob
* Trên thanh menu bar, click `Assets > Google Mobile Ads/ Settings ...` để tiến hành cài đặt Admob.
* Điền `App ID` tương ứng có định dạng `ca-app-pub-xxxxxxxxxxxxxxxx~yyyyyyyyyy`, lưu ý không cần tích vào các mục bên dưới:

  ![image](resources/images/2_config_key_admob.png)

### Bước 3 - Setup UMP
*  Kéo thả Prefab UMPManager trong folder UMP vừa mới import vào Scene (Scene ngoài cùng, cùng với những Manager khác).

* Chỉ cần quan tâm tới hàm InitUMP của class UMPManager.cs
  ```csharp
    UMPManager.Instance.InitUMP(() =>
    {
        MediationManager.Instance.Init();
        AppOpenAdManager.Instance.Init();
    }, () =>
    {
        Time.timeScale = 0;
    }, () =>
    {
        Time.timeScale = 1;
    });
  ```
* Cách sử dụng hàm InitUMP:
  * `InitUMP` ở đầu game.
  * `callback` là để xử lý Init các Mediation, AOA,...
  * `pauseFunc` là để xử lý dừng game khi popup UMP được show lên.
  * `resumeFunc` là để xử lý tiếp tục Game khi popup UMP đã được Consent.

### Bước 4 - Test UMP
Để thực hiện test việc tích hợp UMP, developer thực hiện theo các chỉ dẫn sau đây:
* Lấy ID test theo hướng dẫn [testing](https://developers.google.com/admob/unity/privacy#testing). Lưu ý, đối với những trường hợp ứng dụng đã lên Store và UMP đang tắt thì không test trực tiếp bằng ID thật thì sẽ dùng ID test của Admob để test (ca-app-pub-3940256099942544~3347511713).

* Bật Debug lên để test trường hợp đang nằm trong vùng EU.
* Thêm tích ở `Is Debug` thêm ID test vào mục `Test Device Ids` ở bên dưới.

  ![images](resources/images/4_test_ump.png)

## Lưu ý khác
Đối với nền tảng iOS, vẫn thực hiện setup như bên Android nhưng cần lưu ý ATT cần được gọi ở `Awake` hoặc trước khi gọi vào hàm `Init` của `UMPManager.cs`.

## Release Notes
### Version 2.0 - 14/10/2024
- Update lại logic Init của UMP cho IOS.
- Cập nhật Admob SDK phiên bản mới nhất(9.2.1).
### Version 1.0 - 11/12/2023


## Hỗ trợ
Trong trường hợp có bất cứ vấn đề gì cần hỗ trợ vui lòng liên hệ với team Business Development của Bravestars để nhận hỗ trợ.
